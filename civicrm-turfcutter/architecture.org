@startuml
actor user
database civiCRM
control polygon
entity Marker
entity Add_marker
entity Remove_marker
entity Left_menu
entity Right_menu
entity voter_map
entity volunteer_map
entity heat_map
user -> Marker: On click, display information associated with marker.
user -> Add_marker: User adds marker to map.\n If placed markers form a closed polygon then adds data to civiCRM as described below.
user -> Remove_marker: Removes placed marker from the map.\n In the event it is in the middle of a chain of markers in a polygon\n it gets the two adjacent markers and connects them together.
polygon -> civiCRM: On complete polygon, a dropdown menu loads \n requesting which volunteer to be assigned to the turf.\n When the volunteer is selected a group is created in civiCRM.\n The group then has the voters within the turf assigned to it.
user -> Left_menu: On click slides in left menu.
user -> Right_menu: On click slides in right menu.
user -> voter_map: On click changes map to display voter markers.
user -> volunteer_map: On click changes map to display all active volunteer turfs, homes, and places of work.
user -> heat_map: On click displays all voter markers \n color coded corresponding to a user query from a menu.
@enduml
